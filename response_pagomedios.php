<?php 
	require('../../../wp-load.php');
	$order = wc_get_order( $_POST['order_id'] );
	if( $_POST['authorization_result'] == '00' ){
		$order->payment_complete();
		$order->reduce_order_stock();
		WC()->cart->empty_cart();
		wp_redirect( $order->get_checkout_order_received_url() );
	}else{
		wp_redirect( $order->get_cancel_order_url() );
	}
?>
