<?php
/**
 * Plugin Name: WooCommerce Pagomedios Gateway
 * Plugin URI: http://www.pagomedios.com/
 * Description: Integración de Pagomedios para Woocommerce.
 * Author: Pagomedios
 * Author URI: http://www.pagomedios.com/
 * Version: 1.0
 * Text Domain: wc-gateway-pagomedios
 * Domain Path: /i18n/languages/
 *
 * Copyright: (c) 2016-2017 Pagomedios. (info@abitmedia.com) and WooCommerce
 *
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 *
 * @package   WC-Gateway-Pagomedios
 * @author    Abitmedia
 * @category  Admin
 * @copyright Copyright (c) 2016-2017, Abitmedia. and WooCommerce
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 *
 * This offline gateway forks the WooCommerce core "Cheque" payment gateway to create another offline payment method.
 */
 
defined( 'ABSPATH' ) or exit;


// Make sure WooCommerce is active
if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	return;
}


/**
 * Add the gateway to WC Available Gateways
 * 
 * @since 1.0.0
 * @param array $gateways all available WC gateways
 * @return array $gateways all WC gateways + offline gateway
 */
function wc_pagomedios_add_to_gateways( $gateways ) {
	$gateways[] = 'WC_Gateway_Pagomedios';
	return $gateways;
}
add_filter( 'woocommerce_payment_gateways', 'wc_pagomedios_add_to_gateways' );


/**
 * Adds plugin page links
 * 
 * @since 1.0.0
 * @param array $links all plugin links
 * @return array $links all plugin links + our custom links (i.e., "Settings")
 */
function wc_pagomedios_gateway_plugin_links( $links ) {

	$plugin_links = array(
		'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=offline_gateway' ) . '">' . __( 'Configure', 'wc-gateway-offline' ) . '</a>'
	);

	return array_merge( $plugin_links, $links );
}
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'wc_pagomedios_gateway_plugin_links' );


/**
 * Offline Payment Gateway
 *
 * Provides an Offline Payment Gateway; mainly for testing purposes.
 * We load it later to ensure WC is loaded first since we're extending it.
 *
 * @class 		WC_Gateway_Pagomedios
 * @extends		WC_Payment_Gateway
 * @version		1.0.0
 * @package		WooCommerce/Classes/Payment
 * @author 		SkyVerge
 */
add_action( 'plugins_loaded', 'wc_pagomedios_gateway_init', 11 );

function wc_pagomedios_gateway_init() {

	class WC_Gateway_Pagomedios extends WC_Payment_Gateway {

		/**
		 * Constructor for the gateway.
		 */
		public function __construct() {
	  
			$this->id                 = 'offline_gateway';
			$this->icon               = plugin_dir_url( __FILE__ ).'images/tarjetas.png';
			$this->has_fields         = false;
			$this->method_title       = __( 'Redypago', 'wc-gateway-offline' );
			$this->method_description = __( 'Configure su tienda en línea WooCommerce para soportar pagos en línea con Redypago.', 'wc-gateway-offline' );
		  
			// Load the settings.
			$this->init_form_fields();
			$this->init_settings();
		  
			// Define user set variables
			$this->title        = 'Tarjeta de crédito / débito';
			$this->description  = 'Pago seguro en línea con Redypago';
			$this->instructions = 'Instrucciones';
		  
			// Actions
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
			add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thankyou_page' ) );
		  
			// Customer Emails
			add_action( 'woocommerce_email_before_order_table', array( $this, 'email_instructions' ), 10, 3 );
		}
	
	
		/**
		 * Initialize Gateway Settings Form Fields
		 */
		public function init_form_fields() {
	  
			$this->form_fields = apply_filters( 'wc_offline_form_fields', array(
		  
				'enabled' => array(
					'title'   => __( 'Activar/Desactivar', 'wc-gateway-offline' ),
					'type'    => 'checkbox',
					'label'   => __( 'Activar Pagomedios', 'wc-gateway-offline' ),
					'default' => 'yes'
				),
				
				'commerce_id' => array(
					'title'       => __( 'Comercio ID', 'wc-gateway-offline' ),
					'type'        => 'text',
					'description' => __( 'Ingrese su Comercio ID proporcionado por Pagomedios.', 'wc-gateway-offline' ),
					'default'     => __( '', 'wc-gateway-offline' ),
					'desc_tip'    => true,
				),
				
			) );
		}
	
	
		/**
		 * Output for the order received page.
		 */
		public function thankyou_page() {
			if ( $this->instructions ) {
				echo wpautop( wptexturize( $this->instructions ) );
			}
		}
	
	
		/**
		 * Add content to the WC emails.
		 *
		 * @access public
		 * @param WC_Order $order
		 * @param bool $sent_to_admin
		 * @param bool $plain_text
		 */
		public function email_instructions( $order, $sent_to_admin, $plain_text = false ) {
		
			// if ( $this->instructions && ! $sent_to_admin && $this->id === $order->payment_method && $order->has_status( 'on-hold' ) ) {
			// 	echo wpautop( wptexturize( $this->instructions ) ) . PHP_EOL;
			// }
		}
	
	
		/**
		 * Process the payment and return the result
		 *
		 * @param int $order_id
		 * @return array
		 */
		public function process_payment( $order_id ) {
			$order = new WC_Order( $order_id );

			$customer_id = $order->get_user_id();

			if( $customer_id == 0 ){
				$customer_id = str_pad( rand(1, 999999999), 9, "0", STR_PAD_LEFT );
			}

			$url = 'https://app.redypago.com/api/setorder/'; //URL del servicio web REST
			// $url = 'http://localhost/pagomedios/web/api/setorder/';
			$header = array( 'Content-Type: application/json' );
			$dataOrden = array(	'commerce_id' => $this->get_option( 'commerce_id' ), //ID unico por comercio
								'customer_id' => $customer_id, //Identificación del tarjeta habiente (RUC, Cédula, Pasaporte)
								'customer_name' => $order->billing_first_name, //Nombres del tarjeta habiente
								'customer_lastname' => $order->billing_last_name, //Apellidos del tarjeta habiente
								'customer_phones' => $order->billing_phone,  //Teléfonos del tarjeta habiente
								'customer_address' => $order->billing_address_1,  //Dirección del tarjeta habiente
								'customer_email' => $order->billing_email,  //Correo electrónico del tarjeta habiente
								'customer_language' => 'es',  //Idioma del tarjeta habiente
								'order_description' => 'Pago desde '.get_bloginfo('name'),  //Descripción de la órden
								'order_amount' => $order->total, //Monto total de la órden
								'order_id' => $order_id, 
								'response_url' => plugin_dir_url( __FILE__ ).'response_pagomedios.php', 
							);

			$params = http_build_query( $dataOrden ); //Tranformamos un array en formato GET
			 //Consumo del servicio Rest
		    $curl = curl_init();
		    curl_setopt($curl, CURLOPT_URL, $url.'?'.$params);
		    curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		    $response = curl_exec($curl);
		    curl_close($curl);
		    
		    $responsePagomedios = json_decode($response);
		    if( $responsePagomedios->status == 1 ){
		    	return array(
					'result' 	=> 'success',
					'redirect'	=> $responsePagomedios->data->payment_url,
				);
		    }else{
		    	return array(
					'result' 	=> 'failure',
					'refresh' => false,
					'reload' => false,
					// 'redirect'	=> $responsePagomedios->data->payment_url,
				);
		    }

		}
	
  }
}